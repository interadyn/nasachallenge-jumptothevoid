﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public delegate void PlayerReturn(int playerId, int result, bool stopped);


public enum GameMode
{
    Jumper,
    Ball
}


public class GameController : MonoBehaviour {
    
    public enum GameState
    {
        Init,
        ModeSelection,
        Playing,
        ShowingScores,
        Reset
    }

    [SerializeField]
    private GameObject playerPrefab;

    [SerializeField]
    private Transform gameModeParent;

    private Transform circlePlayerInstance;

    private DataController.UserData activeUser;
    private PlayerController activePlayerController;

    public static GameController instance;
    public static GameController Instance
    {
        get
        { 
            return instance;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void StartNewGame(GameMode gameMode)
    {
        if (gameMode == GameMode.Jumper)
        {
            DataController.Instance.RegisterNewUser(0);
            DataController.Instance.RegisterNewUser(1);

            DataController.Instance.UpdateUserInfo(0, UiController.Instance.GetRandomColorFromDB());
            DataController.Instance.UpdateUserInfo(1, UiController.Instance.GetRandomColorFromDB());

            DataController.Instance.NewGame();  
            
            StartCoroutine(PreGameCoroutine());
        }
    }

    private IEnumerator PreGameCoroutine()
    {
        SoundManager.PlaySFX("GetReady");
        UiController.Instance.AnimateText("Get Ready");

        yield return new WaitForSeconds(1f);

        NewRound(DataController.Instance.getNextUser());
    }

    public void RoundCallBack(int playerId, int result, bool stopped)
    {
        activePlayerController.Destroy();
        DataController.Instance.SetUserPoints(playerId, result);
        UiController.Instance.AnimateText(result.ToString());
        UiController.Instance.UpdatePlayerPoints(playerId, result);
        StartCoroutine(AfterRoundCoroutine());
    }

    private IEnumerator AfterRoundCoroutine()
    {
        yield return new WaitForSeconds(3f);

        NewRound(DataController.Instance.getNextUser());
    }

    public void NewRound(DataController.UserData user)
    {
        GameObject obj = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        circlePlayerInstance = obj.transform;
        circlePlayerInstance.SetParent(gameModeParent);
        circlePlayerInstance.localPosition = Vector2.zero;
        circlePlayerInstance.localScale = Vector2.one;
        activePlayerController = obj.GetComponent<PlayerController>();
        activeUser = user;
        Debug.Log(activeUser.ID);
        activePlayerController.Init(RoundCallBack, activeUser.ID, activeUser.AssignedColor);
        activePlayerController.StartRound(Random.Range(2.9f, 4.5f));
    }
}
